const GeoCodeService = require('./geocode');
const NearestService = require('./nearest');
const flatten = require('./../util/flatten');


class CoordinatorService{
    constructor(locations, requestIp){
        this.errors = [];
        this.errorCode = null;
        this.locations = locations;
        this.requestIp = requestIp;
        this.geoCodeResponse = null;
        this.nearestLocations = null;
    }

    async invoke(){
        await this.geoCode();
        await this.findNearest();

        return (new Promise((resolve, reject) => {
            resolve(new CoordinatorResponse(this.errors, this.errorCode, this.nearestLocations));
        }))
    }

    async geoCode() {
        let geoCodeResponse = await(new GeoCodeService(this.locations).invoke());

        if (!geoCodeResponse.isSuccess()){
            this.errors.push(geoCodeResponse.errors)
            this.errors = flatten(this.errors)
            this.errorCode = 'geocode_service_failure'
        } else {
            this.geoCodeResponse = geoCodeResponse;
        }
    }

    async findNearest() {
        if(this.errors.length > 1) {
            return
        }

        let nearestServiceResponse = await(new NearestService(this.geoCodeResponse.geoCodedLocations, this.requestIp).invoke());

        if (!nearestServiceResponse.isSuccess()) {
            this.errors.push(nearestServiceResponse.errors);
            this.errors = flatten(this.errors);
            this.errorCode = 'nearest_service_failure'
        } else {
            this.nearestLocations = nearestServiceResponse.sortedLocations;
        }
    }
}

class CoordinatorResponse{
    constructor(errors, errorCode, nearestLocations) {
        this.errors = errors;
        this.errorCode = errorCode;
        this.nearestLocations = nearestLocations
    }

    isSuccess() {
        return (this.errors.length) > 1 ? false : true
    }

}

module.exports = CoordinatorService;