const go = require("./../util/go");
const geoip = require('geoip-lite');
const haversine = require('haversine');
const Location = require('./../models/location');

class NearestService {
    constructor(locations, requestIp) {
        this.errors = []
        this.errorCode = null;
        this.locations = locations;
        this.requestIp = requestIp;
        this.requestLocation = null;
    }

    async invoke() {
        await this.getRequestIpLatLng();
        await this.compareDistanceFromLocations();
        await this.sortDistances();

        return (new Promise((resolve, reject) => {
            resolve(new NearestServiceResponse(this.errors, this.errorCode, this.locations));
        }));
    }

    getRequestIpLatLng() {
        let ipLocation = geoip.lookup(this.requestIp);

        if (ipLocation) {
            let [lattitude, longitude] = ipLocation.ll
            this.requestLocation = new Location(lattitude, longitude, ipLocation.country, ipLocation.city, ipLocation.state, null);
        } else {
            this.errorCode = 'request_location_could_not_be_determined'
            this.errors.push(new Error('Request location could not be determined'));
        }
    }

    compareDistanceFromLocations() {
        if (this.errors.length > 0) {
            return
        }

        let srcLocation = {
            latitude: this.requestLocation.lat,
            longitude: this.requestLocation.lon
        }

        this.locations.map((loc) => {
            let destLocation = {latitude: loc.lat, longitude: loc.lon};
            let distanceImMeters = haversine(srcLocation, destLocation, {unit: 'meter'})
            loc.setDistanceFromSource(distanceImMeters);
            return loc;
        })
    }

    sortDistances(){
        if (this.errors.length > 0) {
            return
        }

        this.locations.sort((loc1, loc2) => {
            if (loc1.distanceFromSourceLocation < loc2.distanceFromSourceLocation) return -1
            if (loc1.distanceFromSourceLocation > loc2.distanceFromSourceLocation) return +1
            return 0;
        })
    }
}

class NearestServiceResponse{
    constructor(errors, errorCode, locations) {
        this.errors = errors;
        this.errorCode = errorCode
        this.sortedLocations = locations;
    }

    isSuccess() {
        return (this.errors.length > 0) ? false : true;
    }
}

module.exports = NearestService;