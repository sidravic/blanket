const geocoder_client = require("./../config/geocoder");
const go = require("./../util/go");
const defaultCountry = process.env.DEFAULT_COUNTRY || 'India'
const Location = require('./../models/location');

const prepareLocations = (locations) => {
    let preparedLocations = locations.map((loc) => {
        return {address: loc, minConfidence: 0.9, countryCode: defaultCountry};
    });

    return preparedLocations;
}

class GeoCodeService {
    constructor(locations) {
        this.locations = locations;
        this.client = geocoder_client;
        this.errors = [];
        this.errorCode = null;
        this.openCageResponse = null;
        this.geoCodedLocations = []
    }

    async invoke() {
        await this.getLatLng();
        await this.parseResponse();

        return (new Promise((resolve, reject) => {
            resolve(new GeoCodeServiceResponse(this.errors, this.errorCode, this.geoCodedLocations));
        }))

    }

    async getLatLng() {
        const locationOptions = prepareLocations(this.locations);

        let [err, opencageResponse] = await go(
            this.client.batchGeocode(locationOptions)
        );

        if (err) {
            this.errors.push(err.message);
            this.errorCode = 'geocode_api_failure';
            return;
        }

        this.openCageResponse = opencageResponse;
    }

    parseResponse() {
        if (this.errors.length > 0) {
            return;
        }

        const validResponses = this.openCageResponse.map((response, index) => {
            if (!response.error) {
                let location = response.value[0];
                return (new Location(location.latitude, location.longitude, location.country, location.city, location.state, location.zipcode, this.locations[index]))
            }
        })

        this.geoCodedLocations = validResponses;
    }
}

class GeoCodeServiceResponse {
    constructor(errors, errorCode, geoCodedLocations) {
        this.errors = errors;
        this.errorCode = errorCode;
        this.geoCodedLocations = geoCodedLocations;
    }

    isSuccess() {
        return ((this.errors.length > 0) ? false : true)
    }
}


module.exports = GeoCodeService;
