const flatten = (arr) => {
    return [].concat.apply([], arr)
}

module.exports = flatten;