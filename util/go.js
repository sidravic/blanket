const go = async func => {
  try {
    let value = await func;
    return [null, value];
  } catch (e) {          
    return [e.message, null];    
  }
};

module.exports = go;
