const log = require('./../config/logger');

const requestLogger = (req, res, next) => {
    log.info({
        method: req.method,
        path: req.path,
        params: req.params,
        query: req.query,
        request_ip: req.ips,
        body: req.body    
    })

    next();
}

module.exports = requestLogger