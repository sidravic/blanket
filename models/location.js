class Location {
    constructor(lat, lon, country, city, state, zipcode, name) {
        this.lat = lat;
        this.lon = lon;
        this.country = country;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.name = name;
        this.distanceFromSourceLocation = null;
    }

    setDistanceFromSource(distanceInMeters) {
        this.distanceFromSourceLocation = distanceInMeters;
    }
}

module.exports = Location