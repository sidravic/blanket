### Installation

The easiest installation would be to run pull the docker image from supersid/blanket

```$xslt
docker pull supersid/blanket
docker run -it --env-file development.env --add-host dockerhost:10.10.10.10 -p 3799:3799 supersid/blanket:latest
```

### Without Docker

Clone the code and run npm install  
```$xslt
 $ git clone git@gitlab.com:sidravic/blanket.git
 $ cd blanket
 $ npm install
```

The application expects certain environment variables to be present. These are described in the env.sample file

```$xslt
NODE_ENV=production
HOSTNAME=0.0.0.0
PORT=3799
OCD_API_KEY=avalidkey
```

The `OCD_API_KEY` refers to the `opencagedata` used for geocoding locations. 

### Assumptions

1. The code does not run any request parameter validation at this point.
2. The emphasis has been to structure code in a reusable manner and the pattern allows each service to operate independent of others.
3. The amount of effort/time needed to throw away code is low as they're highly decoupled.
4. There is little effort spent on optimising the performance of the application and this could be improved in several ways.
  
 

### Sample Request

```$xslt
curl -X POST \
  https://blanket-1.herokuapp.com/api/v1/locations \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 53554cfa-5659-c2fd-a61d-8ce7e72ad41f' \
  -d '{
	"locations": ["Statue of Liberty", "1 Hacker Way, California",  "Nahar Amrit Shakti, Chandivali", "Columbo, Srilanka"]
}'
```


