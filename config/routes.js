const express = require('express');
const router = express.Router();
const WelcomeController = require('../controllers/api/v1/welcome_controller');
const LocationsController = require('../controllers/api/v1/locations_controller');


router.get('/', WelcomeController.Index)
router.get('/api/v1/', WelcomeController.Show)
router.post('/api/v1/locations', LocationsController.Create)

module.exports = router