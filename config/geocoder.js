const NodeGeocoder = require('node-geocoder');

const options = {
    provider: 'opencage',
    apiKey: process.env.OCD_API_KEY,
    httpAdapter: 'request',
    formatter: null
}

const geocoder = NodeGeocoder(options);

module.exports = geocoder;