const winston = require('winston');

const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.combine(        
        winston.format.json(),
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
    ),    
    transports: [      
      new winston.transports.Console({
          format: winston.format.json(),
      })
    ]
  });
  

logger.info('Logger initialized.');
module.exports = logger;

