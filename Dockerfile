FROM node:10.16

# Create app directory
RUN mkdir -p /blanket
WORKDIR /blanket

# Install app dependencies
COPY package.json /blanket
COPY package-lock.json /blanket
RUN npm install --production


# Bundle app source
COPY . /blanket
RUN rm .envrc

CMD [ "node", "app.js" ]
