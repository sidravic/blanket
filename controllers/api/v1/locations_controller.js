const {successResponse, failureResponse, badRequest, unauthorized, notFound} = require('./base_controller');
const CoordinatorService = require('./../../../services/coordinator');

module.exports.Create = async (req, res) => {

    let requestIp = req.headers['x-forwarded-for']

    if (!requestIp) {
        let responseObject = badRequest('unable to determine request ip');
        res.status(400).json(responseObject);
        return
    }

    let coordinatorResponse = await (new CoordinatorService(req.body.locations, requestIp).invoke())
    if (coordinatorResponse.isSuccess()) {
        let responseObject = successResponse({nearestLocations: coordinatorResponse.nearestLocations});
        res.status(200).json(responseObject);
    } else {
        let responseObject = failureResponse(null, coordinatorResponse.errors, coordinatorResponse.errorCode)
        res.status(200).json(responseObject);
    }
};
