const { successResponse, failureResponse, badRequest, unauthorized, notFound } = require('./base_controller')


module.exports.Index = (req, res) => {
    let payload = {hello: 'world'}
    let responseObject = successResponse(payload)

    res.status(200).json(responseObject)
}


module.exports.Show = (req, res) => {
    let errorMessages = ['Bad Request'];
    let responseObject = badRequest(errorMessages)

    res.status(400).json(responseObject);
}
