echo "${DOCKERHUB_PASSWORD}" | docker login -u "${DOCKERHUB_USERNAME}" --password-stdin

docker build -t blanket:latest .
timestamp=$(awk 'BEGIN {srand(); print srand()}')

docker tag blanket:latest supersid/blanket:$timestamp
docker push supersid/blanket:$timestamp